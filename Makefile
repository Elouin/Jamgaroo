VENV = venv
PYTHON = $(VENV)/bin/python
PIP = $(VENV)/bin/pip

$(VENV)/bin/activate:
	python3 -m venv $(VENV)
	
dependencies: $(VENV)/bin/activate
	$(PIP) install -e .

interactive:
	$(PYTHON)

run: dependencies
	FLASK_DEBUG=1 FLASK_ENV=development FLASK_APP=jamgaroo $(PYTHON) -m flask run

run_container:
	podman run -p 8000:8000 -e FLASK_ENV=development -e FLASK_DEBUG=1 $$(podman build -q .)

clean:
	find . -name "*.pyc" -delete
	find . -name "__pycache__" -delete
	rm -rf Jamgaroo.egg-info
	rm -rf $(VENV)
	rm -rf build/
