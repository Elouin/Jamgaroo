# Jamgaroo

Manage gamejams and their submissions.

Jamgaroo is supposed to be as minimal as possible and do only what it needs to to serve its purpose.

## Features

* one time jams and jam series
* user profiles with games and jams listed
* integrated voting system
* no javascript
* no file hosting
* markdown support for description text

## Deployment

There is no predefined way of running it in production just yet(in general not recommended to do so yet), to run a local copy simply clone the repo and then:
```
make run
```

### Configuration

There is an example `jamgaroo.cfg` file in the repo. Copy that and make adjustments as needed, then set the environment variable `JAMGAROO_SETTINGS` to the path to your configuration. Also set the environment variable `JAMGAROO_DB` to the path where you want the db to be stored.

Alternatively you can set each of the values in the example configuration file by prefixing `JAMGAROO_` as environment variables.

**Options**

| Name | Default | Description |
|---|---|---|
| NAME | "Jamgaroo" | Name of your jamgaroo installtion |
| ADMINS | ["Admin"] | List of usernames that have admin rights |
| ABOUT | see example conf | Text shown on the "about" page |
| BLOCKED_USERNAMES | ["admin", "jamgaroo", "webmaster"] | List of usernames that cant be registered |
| DESCRIPTION | see example conf | Short description shown on index page |
| FOOTER | see example conf | Text shown at the bottom of each page |
| REGISTRATION_DISABLED | False | If True Registration will be disabled |
| SECRET_KEY | b's3cr3t' | See [Flask Documentation](https://flask.palletsprojects.com/en/2.2.x/config/?highlight=secret%20key#SECRET_KEY). CHANGE THIS! |
| TOS | see example conf | Terms of Service, shown on the "tos" page |

## Contributing

Contributions welcome! If you want to contribute, please look for an issue on the things you want to contribute and if there is none, please create one.
Then fork the repository and create a merge request.
Please add typing header and docstrings to your functions.

