import re

from flask import render_template, request, redirect, url_for, session, Blueprint, abort, current_app, flash
from flask.typing import ResponseReturnValue
from passlib.hash import pbkdf2_sha256
from peewee import fn

from jamgaroo import models, wrapper


users = Blueprint('users', __name__, template_folder='templates')


@users.route("/<user_name>")
def show_user(user_name: str) -> ResponseReturnValue:
    """
    Renders users profile with a list of their games.

    :param user_name: username of the user to show details
    :tupe user_name: str

    :return: rendered template of user profile
    :rtype: ResponseReturnValue
    """
    if not models.User.select().where(models.User.name == user_name).exists():
        abort(404)

    user = models.User.get(models.User.name == user_name)
    user_games = models.Game.select().join(models.User).where(models.User.name == user_name)

    return render_template("user.html", user = user, user_games = user_games)


@users.route("/<user_name>/edit", methods=['GET', 'POST'])
@wrapper.login_required
def edit_user(user_name: str) -> ResponseReturnValue:
    """
    Renders a form for editing user on get and edits user on post.

    :param user_name: id of the user to edit
    :type user_name: str

    :return: rendered template of form for editing of users
    :rtype: ResponseReturnValue
    """
    if not models.User.select().where(models.User.name == user_name).exists():
        abort(404)

    if not models.User.get(models.User.name == user_name).name == session['username']:
        abort(403)

    if request.method == 'GET':
        user = models.User.get(models.User.name == user_name)
        return render_template("edit_user.html", user = user)

    if request.method == 'POST':
        if request.form['name'] and request.form['email']:
            if models.User.select().where(fn.LOWER(models.User.name) == request.form['name'].lower()).exists() and request.form['name'].lower() != models.User.get(models.User.name == user_name).name.lower():
                flash("Updating profile failed, a profile with that name already exists.", "error")
                return render_template("edit_user.html", user = models.User.get(models.User.name == user_name), user_name = user_name)
            if request.form['name'].lower() in list(map(str.lower, current_app.config['BLOCKED_USERNAMES'])):
                flash("Updating profile failed, username is not allowed!", "error")
                return render_template("edit_user.html", user = models.User.get(models.User.name == user_name), user_name = user_name)
            if not re.match(r"^[0-9a-zA-Z\-]{3,12}$", request.form['name']):
                flash("Updating profile Failed, username must only contain letters, numbers and '-' and be between 3 and 12 chars long.", "error")
                return render_template("edit_user.html", user = models.User.get(models.User.name == user_name), user_name = user_name)
            models.User.update(name = request.form['name'], \
                    email = request.form['email'], \
                    description = request.form['description'] \
                    ).where(models.User.name == user_name).execute()
            session.clear()
            session['username'] = request.form['name']
            flash("User successfully edited!", "success")
            return render_template("edit_user.html", user = models.User.get(models.User.name == request.form['name']))
        flash("Updating profile failed, please enter a name", "error")
        return render_template("edit_user.html", user = models.User.get(models.User.name == user_name))


@users.route("/<user_name>/delete", methods=['GET', 'POST'])
@wrapper.login_required
def delete_user(user_name: str) -> ResponseReturnValue:
    """
    Deletes a user.

    :param user_id: id of the user to delete
    :type user_id: int

    :return: rendered template of confirmation form for user deletion
    :rtype: ResponseReturnValue
    """
    if not models.User.select().where(models.User.name == user_name).exists():
        abort(404)

    if not models.User.get(models.User.name == user_name).name == session['username']:
        if not session['username'] in current_app.config["ADMINS"]:
            abort(403)

    if request.method == 'GET':
        user = models.User.get(models.User.name == user_name)
        return render_template("delete_user.html", user = user)

    if request.method == 'POST':
        models.User.delete().where(models.User.name == user_name).execute()
        current_app.logger.info(f"user {user_name} deleted by {session['username']}.")
        session.clear()
        flash("User successfully deleted!", "success")
        return redirect(url_for('index'))


@users.route("/<user_name>/password_change", methods=['GET', 'POST'])
@wrapper.login_required
def password_change(user_name: str) -> ResponseReturnValue:
    """
    Renders a form for changing a password and changes it on post.

    :param user_name: id of the user to change the password for
    :type user_name: str

    :return: rendered template of form for changing the password
    :rtype: ResponseReturnValue
    """
    if not models.User.select().where(models.User.name == user_name).exists():
        abort(404)

    if not models.User.get(models.User.name == user_name).name == session['username']:
        abort(403)

    if request.method == 'GET':
        user = models.User.get(models.User.name == user_name)
        return render_template("change_password.html", user = user)

    if request.method == 'POST':
        if 'current-password' in request.form and 'new-password-1' in request.form and 'new-password-2' in request.form:
            user = models.User.get(models.User.name == session['username'])
            if pbkdf2_sha256.verify(request.form['current-password'], user.password) and request.form['new-password-1'] == request.form['new-password-2']:
                user.password = pbkdf2_sha256.hash(request.form['new-password-2'])
                user.save()
                flash("Changed password successfully!", "success")
                return redirect(url_for('users.show_user', user_name = user.name))
            flash("Changing password failed, wrong current password or passwords didn't match!", "success")
            return render_template("change_password.html")
        flash("Changing password failed, please fill out all fields!", "success")
        return render_template("change_password.html", user = models.User.get(models.User.name == user_name))
