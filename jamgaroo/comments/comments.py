import datetime

from flask import request, redirect, url_for, session, render_template, Blueprint, current_app, abort, flash
from flask.typing import ResponseReturnValue
from peewee import JOIN

from jamgaroo import models, wrapper


comments = Blueprint('comments', __name__, template_folder='templates')


@comments.route("/<int:comment_id>")
def show_comment(comment_id: int) -> ResponseReturnValue:
    """
    Renders a page that shows a comment and its rating.

    :param comment_id: id of the comment to show
    :type comment_id: int

    :return: rendered template of comment details
    :rtype: ResponseReturnValue
    """
    if not models.Comment.select().where(models.Comment.id == comment_id).exists():
        abort(404)

    comment = models.Comment.select().join(models.User).where(models.Comment.id == comment_id).get()

    ratings = models.Rating.select().join(models.Category).join_from(models.Rating, models.User).where(models.Rating.game == comment.game, models.User.name == comment.user.name)

    user_ratings = {}

    for rating in ratings:
        user_ratings[rating.category.name] = rating.points
    return render_template("comment.html",
            user_ratings = user_ratings,
            comment = comment,
            current_date = datetime.datetime.now())


@comments.route("/<comment_id>/edit", methods=['GET', 'POST'])
@wrapper.login_required
def edit_comment(comment_id: int) -> ResponseReturnValue:
    """
    Edits a comment.

    :param comment_id: id of the comment to edit
    :type comment_id: int

    :return: rendered template of form for editing of comment
    :rtype: ResponseReturnValue
    """
    if not models.Comment.select().where(models.Comment.id == comment_id).exists():
        abort(404)

    if not models.Comment.select().join(models.User).where(models.Comment.id == comment_id).get().user.name == session['username']:
        abort(403)

    comment = models.Comment.select().join(models.User).where(models.Comment.id == comment_id).get()
    ratings = models.Rating.select().join(models.Category).join_from(models.Rating, models.User).where(models.Rating.game == comment.game, models.User.name == comment.user.name)

    if request.method == 'GET':
        return render_template("edit_comment.html", comment = comment, ratings = ratings)

    if request.method == 'POST':
        game = models.Game.get(models.Game.id == comment.game)
        jam_categories = models.JamCategory.select().join(models.Category, JOIN.LEFT_OUTER) \
                .where(models.JamCategory.jam == game.jam)
        category_names = [category.category.name for category in jam_categories]
        for item in request.form:
            if request.form[item]:
                if item == 'comment':
                    comment.comment = request.form[item]
                    comment.save()
                if item in category_names:
                    rating = models.Rating.select().join(models.Category).join_from(models.Rating, models.User).where(models.Rating.game == comment.game, models.User.name == comment.user.name, models.Category.name == item).get()
                    rating.points = request.form[item]
                    rating.save()
            else:
                flash("Editing review failed, please fill out all fields.", "error")
                return render_template("edit_comment.html", comment = comment, ratings = ratings)
        current_app.logger.info(f"Game rating {comment_id} edited by {session['username']}.")
        flash("Rating successfully edited!", "success")
        return redirect(url_for('games.show_game', game_id = comment.game.id))


@comments.route("/<int:comment_id>/delete", methods=['GET', 'POST'])
@wrapper.login_required
def delete_comment(comment_id: int) -> ResponseReturnValue:
    """
    Deletes a comment.

    :param comment_id: id of the comment to delete
    :type comment_id: int

    :return: rendered template of confirmation form for comment deletion
    :rtype: ResponseReturnValue
    """
    if not models.Comment.select().where(models.Comment.id == comment_id).exists():
        abort(404)

    if not models.Comment.select().join(models.User).where(models.Comment.id == comment_id).get().user.name == session['username']:
        abort(403)

    comment = models.Comment.select().join(models.User).where(models.Comment.id == comment_id).get()

    if request.method == 'GET':
        return render_template("delete_comment.html", comment = comment)

    if request.method == 'POST':
        models.Comment.delete().where(models.Comment.id == comment_id).execute()
        # models.Rating.delete().join(models.Category).join_from(models.Rating, models.User).where(models.Rating.game == comment.game, models.User.name == comment.user.name).execute()
        ratings = models.Rating.select().join(models.Category).join_from(models.Rating, models.User).where(models.Rating.game == comment.game, models.User.name == comment.user.name)
        for rating in ratings:
            rating.delete_instance()
        current_app.logger.info(f"Comment {comment.id} deleted by {session['username']}.")
        flash("Successfully deleted comment!", "success")
        return redirect(url_for('games.show_game', game_id = comment.game.id))

    
@comments.route("/<int:comment_id>/report", methods=['GET','POST'])
@wrapper.login_required
def report_comment(comment_id: int) -> ResponseReturnValue:
    """
    Report a comment

    :param comment_id: id of the comment to report
    :type comment_id: int

    :return: rendered template of form to report the comment
    :rtype: ResponseReturnValue
    """
    if not models.Comment.select().where(models.Comment.id == comment_id).exists():
        abort(404)

    comment = models.Comment.get(models.Comment.id == comment_id)

    if request.method == 'GET':
        return render_template("report_comment.html", comment_id = comment_id, comment = comment)

    if request.method == 'POST':
        if request.form['reason']:
            current_app.logger.info(f"Comment {comment.id} was reported by {session['username']}")
            models.CommentReport.create(comment = comment_id, \
                    reason = request.form['reason'], \
                    reported_by = models.User.get(models.User.name == session['username']).id)
            flash("Successfully reported this comment!", "success")
            return redirect(url_for('comments.show_comment', comment_id = comment_id))
        flash("Reporting comment failed, please provide a reason for reporting.", "error")
        return render_template("report_comment.html", comment_id = comment_id, comment=comment)
