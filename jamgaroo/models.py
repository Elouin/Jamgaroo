import datetime
import os

import peewee as pw


db_path = 'data.db'

try:
    db_path = os.environ['JAMGAROO_DB']
except:
    pass

db = pw.SqliteDatabase(db_path, pragmas={'foreign_keys': 1,})


class BaseModel(pw.Model):
    """
    Base model that contains the database connection.
    """
    class Meta:
        """
        define the database for the models
        """
        database = db


class User(BaseModel):
    """
    represents one user in the database
    """
    name = pw.CharField()
    password = pw.CharField()
    email = pw.CharField()
    description = pw.TextField(null = True)
    joined = pw.DateTimeField(default=datetime.datetime.now)
    activated = pw.CharField(default="True")


class Series(BaseModel):
    """
    represents one series in the database
    """
    name = pw.CharField()
    description = pw.TextField()
    created = pw.DateTimeField(default=datetime.datetime.now)
    created_by = pw.ForeignKeyField(User, backref = 'jams', on_delete='CASCADE')


class Jam(BaseModel):
    """
    represents one jam in the database
    """
    name = pw.CharField()
    description = pw.TextField()
    created = pw.DateTimeField(default=datetime.datetime.now)
    created_by = pw.ForeignKeyField(User, backref = 'jams', on_delete='CASCADE')
    start = pw.DateTimeField()
    end_coding = pw.DateTimeField()
    end_testing = pw.DateTimeField()
    series = pw.ForeignKeyField(Series, backref = 'jams', null = True, on_delete='CASCADE')


class Game(BaseModel):
    """
    represents one game in the database
    """
    name = pw.CharField()
    description = pw.TextField()
    link = pw.CharField()
    created = pw.DateTimeField(default=datetime.datetime.now)
    created_by = pw.ForeignKeyField(User, backref = 'games', on_delete='CASCADE')
    jam = pw.ForeignKeyField(Jam, backref = 'games', on_delete='CASCADE')


class Category(BaseModel):
    """
    represents rating categories in the database
    """
    name = pw.CharField()


class JamCategory(BaseModel):
    """
    many to many relationship between jams and categories
    """
    jam = pw.ForeignKeyField(Jam, backref = 'jam_category', on_delete='CASCADE')
    category = pw.ForeignKeyField(Category, backref = 'jam_category', on_delete='CASCADE')


class Comment(BaseModel):
    """
    represents a comment to a game in the database
    """
    comment = pw.TextField()
    user = pw.ForeignKeyField(User, backref = 'comments', on_delete='CASCADE')
    game = pw.ForeignKeyField(Game, backref = 'comments', on_delete='CASCADE')
    created = pw.DateTimeField(default=datetime.datetime.now)


class Rating(BaseModel):
    """
    represents one rating in the database
    """
    points = pw.IntegerField()
    user = pw.ForeignKeyField(User, backref = 'ratings', on_delete='CASCADE')
    game = pw.ForeignKeyField(Game, backref = 'ratings', on_delete='CASCADE')
    category = pw.ForeignKeyField(Category, backref = 'ratings', on_delete='CASCADE')


class JamReport(BaseModel):
    """
    reports on jams
    """
    jam = pw.ForeignKeyField(Jam, backref = 'jam_reports', on_delete='CASCADE')
    reason = pw.TextField()
    reported_by = pw.ForeignKeyField(User, backref = 'jam_reports', on_delete='CASCADE')


class SeriesReport(BaseModel):
    """
    reports on seriess
    """
    series = pw.ForeignKeyField(Series, backref = 'series_reports', on_delete='CASCADE')
    reason = pw.TextField()
    reported_by = pw.ForeignKeyField(User, backref = 'series_reports', on_delete='CASCADE')


class GameReport(BaseModel):
    """
    reports on games
    """
    game = pw.ForeignKeyField(Game, backref = 'game_reports', on_delete='CASCADE')
    reason = pw.TextField()
    reported_by = pw.ForeignKeyField(User, backref = 'game_reports', on_delete='CASCADE')


class CommentReport(BaseModel):
    """
    reports on comments
    """
    comment = pw.ForeignKeyField(Comment, backref = 'comment_reports', on_delete='CASCADE')
    reason = pw.TextField()
    reported_by = pw.ForeignKeyField(User, backref = 'comment_reports', on_delete='CASCADE')

