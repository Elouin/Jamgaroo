from functools import wraps
from typing import Callable, Union, Any

from flask import abort, current_app, redirect, session, url_for
from flask.typing import ResponseReturnValue

def login_required(func: Callable) -> Callable:
    """
    Wrapper function, that checks if the user is logged in.

    :param f: function to wrap
    :type f: Callable

    :return: the wrapped function or redirect to login
    :rtype: Callable or ResponseReturnValue
    """
    @wraps(func)
    def decorated_function(*args: Any, **kwargs: Any) -> Union[Callable, ResponseReturnValue]:
        if 'username' not in session:
            return redirect(url_for('login'))
        return func(*args, **kwargs)
    return decorated_function


def admin_only(func: callable) -> Callable:
    """
    Wrapper function, that checks if the user has admin rights.

    :param f: function to wrap
    :type f: Callable

    :return: the wrapped function or 401
    :rtype: Callable or ResponseReturnValue
    """
    @wraps(func)
    def decorated_function(*args: Any, **kwargs: Any) -> Union[Callable, ResponseReturnValue]:
        if 'username' not in session or not session['username'] in current_app.config["ADMINS"]:
            abort(403)
        return func(*args, **kwargs)
    return decorated_function
