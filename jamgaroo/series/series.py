from datetime import datetime, timezone
import re

from flask import request, redirect, url_for, session, render_template, Blueprint, current_app, abort, Response, flash
from flask.typing import ResponseReturnValue
from feedgen.feed import FeedGenerator
from markdown import markdown
from markupsafe import escape

from jamgaroo import models, wrapper


series = Blueprint('series', __name__, template_folder='templates')


@series.route("/create", methods=['GET', 'POST'])
@wrapper.login_required
def create_series() -> ResponseReturnValue:
    """
    Render page for the creation of series.

    A series has to have a name and description.

    :return: rendered template with input fields for series creation
    :rtype: ResponseReturnValue
    """
    if request.method == 'GET':
        return render_template("create_series.html")

    if request.method == 'POST':
        if request.form['name'] and request.form['description']:
            if models.Series.select().where(models.Series.name == request.form['name']).exists():
                flash("Creating series failed, a series with that name already exists.", "error")
                return render_template("create_series.html")
            if not re.match(r"^[0-9a-zA-Z\-\ ]{3,25}$", request.form['name']):
                flash("Creating series failed, name must only contain letters, numbers and '-' and between 3 and 25 chars long.", "error")
                return render_template("create_series.html")
            created_series = models.Series.create(name=request.form['name'], \
                    description = request.form['description'], \
                    created_by = models.User.get(models.User.name == session['username']).id, \
                    )
            current_app.logger.info(f"Series {request.form['name']} created by {session['username']}.")
            flash("Series created successfully!", "success")
            return redirect(url_for('series.show_series', series_id = created_series.id))
        flash("Creating series failed, please fill out all fields.", "error")
        return render_template("create_series.html")


@series.route("/<series_id>/edit", methods=['GET', 'POST'])
@wrapper.login_required
def edit_series(series_id: int) -> ResponseReturnValue:
    """
    Renders template with fields to edit a series.

    :param series_id: id of the series to edit
    :type series_id: int

    :return: rendered template of form for editing of series
    :rtype: ResponseReturnValue
    """
    if not models.Series.select().where(models.Series.id == series_id).exists():
        abort(404)

    if not models.Series.select().join(models.User).where(models.Series.id == series_id).get().created_by.name == session['username']:
        abort(403)

    if request.method == 'GET':
        current_series = models.Series.get(models.Series.id == series_id)
        return render_template("edit_series.html", current_series = current_series)

    if request.method == 'POST':
        if request.form['name'] and request.form['description']:
            if models.Series.select().where(models.Series.name == request.form['name']).exists() and request.form['name'] != models.Series.get(models.Series.id == series_id).name:
                flash("Editing series failed, a series with that name already exists.", "error")
                return render_template("edit_series.html", current_series = models.Series.get(models.Series.id == series_id), series_id = series_id)
            if not re.match(r"^[0-9a-zA-Z\-\ ]{3,25}$", request.form['name']):
                flash("Editing series failed, name must only contain letters, numbers and '-' and between 3 and 25 chars long.", "error")
                return render_template("edit_series.html", current_series = models.Series.get(models.Series.id == series_id), series_id = series_id)
            models.Series.update(name=request.form['name'], \
                    description = request.form['description'], \
                    ).where(models.Series.id == series_id).execute()
            current_app.logger.info(f"Series {request.form['name']} edited by {session['username']}.")
            flash("Series edited successfully!", "success")
            return redirect(url_for('series.show_series', series_id = series_id))
        flash("Editing series failed, please fill out all fields.", "error")
        return render_template("edit_series.html", current_series = models.Series.get(models.Series.id == series_id), series_id = series_id)


@series.route("/<int:series_id>")
def show_series(series_id: int) -> ResponseReturnValue:
    """
    Renders a page that shows the details of one series.

    :param series_id: id of the series to show
    :type series_id: int

    :return: rendered template of series details
    :rtype: ResponseReturnValue
    """
    if not models.Series.select().where(models.Series.id == series_id).exists():
        abort(404)

    current_series = models.Series.select().join(models.User).where(models.Series.id == series_id).get()

    jams_in_series = models.Jam.select().where(models.Jam.series == series_id)

    return render_template("series.html", current_series = current_series, jams_in_series = jams_in_series)


@series.route("/<int:series_id>/report", methods=['GET','POST'])
@wrapper.login_required
def report_series(series_id: int) -> ResponseReturnValue:
    """
    Report a series

    :param series_id: id of the series to report
    :type series_id: int

    :return: rendered template of form to report the series
    :rtype: ResponseReturnValue
    """
    if not models.Series.select().where(models.Series.id == series_id).exists():
        abort(404)

    series = models.Series.get(models.Series.id == series_id)

    if request.method == 'GET':
        return render_template("report_series.html", series_id = series_id, series = series)

    if request.method == 'POST':
        if request.form['reason']:
            current_app.logger.info(f"Series {series.name} was reported by {session['username']}")
            models.SeriesReport.create(series = series_id, \
                    reason = request.form['reason'], \
                    reported_by = models.User.get(models.User.name == session['username']).id)
            flash("Series successfully reported!", "success")
            return redirect(url_for('series.show_series', series_id = series_id))
        flash("Reporting series failed, please provide a reason for reporting.", "error")
        return render_template("report_series.html", series_id = series_id, series=series)


@series.route("/<int:series_id>/delete", methods=['GET', 'POST'])
@wrapper.login_required
def delete_series(series_id: int) -> ResponseReturnValue:
    """
    Deletes series.

    :param series_id: id of the series to delete
    :type series_id: int

    :return: rendered template of confirmation form for series deletion
    :rtype: ResponseReturnValue
    """
    if not models.Series.select().where(models.Series.id == series_id).exists():
        abort(404)

    if not models.Series.select().join(models.User).where(models.Series.id == series_id).get().created_by.name == session['username']:
        if not session['username'] in current_app.config["ADMINS"]:
            abort(403)

    if request.method == 'GET':
        current_series = models.Series.get(models.Series.id == series_id)
        return render_template("delete_series.html", current_series = current_series)

    if request.method == 'POST':
        models.Series.delete().where(models.Series.id == series_id).execute()
        current_app.logger.info(f"Series with id {series_id} deleted by {session['username']}.")
        flash("Series successfully deleted!", "success")
        return redirect(url_for('index'))


@series.route("/<int:series_id>/jams.rss")
def rss_feed(series_id: int) -> ResponseReturnValue:
    """
    Creates an RSS feed with all the jams from the shown series.

    :param series_id: id of the series to generate an rss feed for
    :type series_id: int

    :return: rss file with all jams in the chosen series
    :rtype: ResponseReturnValue
    """
    series = models.Series.get(models.Series.id == series_id)
    jam_feed = FeedGenerator()
    jam_feed.title(f"{series.name} feed of jams")
    jam_feed.author( {'name':current_app.config['NAME'],'email':''} )
    jam_feed.link( href=request.root_url[:-1] + url_for('series.show_series', series_id = series.id), rel='alternate' )
    jam_feed.link( href=request.root_url[:-1] + url_for('series.rss_feed', series_id = series.id), rel='self' )
    jam_feed.language('en')
    jam_feed.description(f"{series.name} is a series of game jams.")
    for jam in models.Jam.select().where(models.Jam.series == series).order_by(models.Jam.created):
        feed_element = jam_feed.add_entry()
        feed_element.title(jam.name)
        feed_element.link(href=request.root_url[:-1] + url_for('jams.show_jam', jam_id = jam.id))
        feed_element.id(request.root_url[:-1] + url_for('jams.show_jam', jam_id = jam.id))
        feed_element.description(f"{markdown(escape(jam.description))}")
        feed_element.published(jam.created.replace(tzinfo=timezone.utc))
    return Response(jam_feed.rss_str(pretty=True), mimetype='application/rss+xml')
