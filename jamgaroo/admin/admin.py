from flask import abort, render_template, Blueprint, request, current_app, redirect, url_for, session
from flask.typing import ResponseReturnValue
from peewee import fn

from jamgaroo import models, wrapper


admin = Blueprint('admin', __name__, template_folder='templates')

@admin.route("/")
@wrapper.admin_only
def admin_area() -> ResponseReturnValue:
    """
    Render the admin dashboard.

    :return: rendered template of list with all reported items
    :rtype: ResponseReturnValue
    """
    all_reported_jams = models.JamReport \
            .select(models.Jam.name, models.Jam.id, fn.COUNT(models.JamReport.id).alias('number_reports')) \
            .join(models.Jam) \
            .group_by(models.Jam.name, models.Jam.id)

    all_reported_series = models.SeriesReport \
            .select(models.Series.name, models.Series.id, fn.COUNT(models.SeriesReport.id).alias('number_reports')) \
            .join(models.Series) \
            .group_by(models.Series.name, models.Series.id)

    all_reported_games = models.GameReport \
            .select(models.Game.name, models.Game.id, fn.COUNT(models.GameReport.id).alias('number_reports')) \
            .join(models.Game) \
            .group_by(models.Game.name, models.Game.id)

    all_reported_comments = models.CommentReport \
            .select(models.Comment.id, fn.COUNT(models.CommentReport.id).alias('number_reports')) \
            .join(models.Comment) \
            .group_by(models.Comment.id)

    return render_template("dashboard.html",
            all_reported_jams = all_reported_jams, \
            all_reported_series = all_reported_series, \
            all_reported_games = all_reported_games, \
            all_reported_comments = all_reported_comments)


@admin.route("/jam_reports")
@wrapper.admin_only
def reported_jams() -> ResponseReturnValue:
    """
    Render list of all reported jams.

    :return: rendered template of list with all reported items
    :rtype: ResponseReturnValue
    """
    all_reported_jams = models.JamReport \
            .select(models.Jam.name, models.Jam.id, fn.COUNT(models.JamReport.id).alias('number_reports')) \
            .join(models.Jam) \
            .group_by(models.Jam.name, models.Jam.id)

    return render_template("reported_jams.html", all_reported_jams = all_reported_jams)


@admin.route("/jam_reports/<int:jam_id>")
@wrapper.admin_only
def jam_reports(jam_id: int) -> ResponseReturnValue:
    """
    Render a list of all reports for one jam.

    :return: rendered template of a list with all reports for one jam
    :rtype: ResponseReturnValue
    """
    if not models.Jam.select().where(models.Jam.id == jam_id).exists():
        abort(404)

    all_jam_reports = models.JamReport \
            .select() \
            .join(models.Jam) \
            .join(models.User) \
            .where(models.Jam.id == jam_id)

    return render_template("jam_reports.html", all_jam_reports = all_jam_reports)


@admin.route("/jam_reports/<int:jam_id>/<int:report_id>", methods=['GET', 'POST'])
@wrapper.admin_only
def jam_report(jam_id: int, report_id: int) -> ResponseReturnValue:
    """
    Render one report of a jam.

    :return: rendered template of a report for one jam
    :rtype: ResponseReturnValue
    """
    if not models.JamReport.select().where(models.JamReport.jam == jam_id, models.JamReport.id == report_id).exists():
        abort(404)

    jam_report = models.JamReport \
            .select() \
            .join(models.Jam) \
            .join(models.User) \
            .where(models.Jam.id == jam_id, models.JamReport.id == report_id) \
            .get()

    return render_template("jam_report.html", jam_report = jam_report)


@admin.route("/jam_reports/<int:jam_id>/<report_id>/delete", methods=['GET', 'POST'])
@wrapper.admin_only
def delete_jam_report(jam_id: int, report_id: int) -> ResponseReturnValue:
    """
    Deletes a jam Report.

    :param jam_id: id of the jam that was reported
    :type jam_id: int
    :param report_id: id of the jam report to delete
    :type report_id: int

    :return: rendered template of confirmation form for jam report deletion
    :rtype: ResponseReturnValue
    """
    if not models.JamReport.select().where(models.JamReport.jam == jam_id, models.JamReport.id == report_id).exists():
        abort(404)

    if request.method == 'GET':
        jam_report = models.JamReport.get(models.JamReport.id == report_id)
        return render_template("delete_jam_report.html", jam_report = jam_report)

    if request.method == 'POST':
        models.JamReport.delete().where(models.JamReport.id == report_id).execute()
        current_app.logger.info(f"The report with the id {report_id} deleted by {session['username']}.")
        return redirect(url_for('admin.reported_jams'))


@admin.route("/series_reports")
@wrapper.admin_only
def reported_series() -> ResponseReturnValue:
    """
    Render list of all reported series.

    :return: rendered template of list with all reported items
    :rtype: ResponseReturnValue
    """
    all_reported_series = models.SeriesReport \
            .select(models.Series.name, models.Series.id, fn.COUNT(models.SeriesReport.id).alias('number_reports')) \
            .join(models.Series) \
            .group_by(models.Series.name, models.Series.id)

    return render_template("reported_series.html", all_reported_series = all_reported_series)


@admin.route("/series_reports/<int:series_id>")
@wrapper.admin_only
def series_reports(series_id: int) -> ResponseReturnValue:
    """
    Render a list of all reports for one series.

    :return: rendered template of a list with all reports for one series
    :rtype: ResponseReturnValue
    """
    if not models.Series.select().where(models.Series.id == series_id).exists():
        abort(404)

    all_series_reports = models.SeriesReport \
            .select() \
            .join(models.Series) \
            .join(models.User) \
            .where(models.Series.id == series_id)

    return render_template("series_reports.html", all_series_reports = all_series_reports)


@admin.route("/series_reports/<int:series_id>/<int:report_id>", methods=['GET', 'POST'])
@wrapper.admin_only
def series_report(series_id: int, report_id: int) -> ResponseReturnValue:
    """
    Render one report of a series.

    :return: rendered template of a report for one series
    :rtype: ResponseReturnValue
    """
    if not models.SeriesReport.select().where(models.SeriesReport.series == series_id, models.SeriesReport.id == report_id).exists():
        abort(404)

    series_report = models.SeriesReport \
            .select() \
            .join(models.Series) \
            .join(models.User) \
            .where(models.Series.id == series_id, models.SeriesReport.id == report_id) \
            .get()

    return render_template("series_report.html", series_report = series_report)


@admin.route("/series_reports/<int:series_id>/<report_id>/delete", methods=['GET', 'POST'])
@wrapper.admin_only
def delete_series_report(series_id: int, report_id: int) -> ResponseReturnValue:
    """
    Deletes a series Report.

    :param series_id: id of the series that was reported
    :type series_id: int
    :param report_id: id of the series report to delete
    :type report_id: int

    :return: rendered template of confirmation form for series report deletion
    :rtype: ResponseReturnValue
    """
    if not models.SeriesReport.select().where(models.SeriesReport.series == series_id, models.SeriesReport.id == report_id).exists():
        abort(404)

    if request.method == 'GET':
        series_report = models.SeriesReport.get(models.SeriesReport.id == report_id)
        return render_template("delete_series_report.html", series_report = series_report)

    if request.method == 'POST':
        models.SeriesReport.delete().where(models.SeriesReport.id == report_id).execute()
        current_app.logger.info(f"The report with the id {report_id} deleted by {session['username']}.")
        return redirect(url_for('admin.reported_series'))


@admin.route("/game_reports")
@wrapper.admin_only
def reported_games() -> ResponseReturnValue:
    """
    Render list of all reported games.

    :return: rendered template of list with all reported items
    :rtype: ResponseReturnValue
    """
    all_reported_games = models.GameReport \
            .select(models.Game.name, models.Game.id, fn.COUNT(models.GameReport.id).alias('number_reports')) \
            .join(models.Game) \
            .group_by(models.Game.name, models.Game.id)

    return render_template("reported_games.html", all_reported_games = all_reported_games)


@admin.route("/game_reports/<int:game_id>")
@wrapper.admin_only
def game_reports(game_id: int) -> ResponseReturnValue:
    """
    Render a list of all reports for one game.

    :return: rendered template of a list with all reports for one game
    :rtype: ResponseReturnValue
    """
    if not models.Game.select().where(models.Game.id == game_id).exists():
        abort(404)

    all_game_reports = models.GameReport \
            .select() \
            .join(models.Game) \
            .join(models.User) \
            .where(models.Game.id == game_id)

    return render_template("game_reports.html", all_game_reports = all_game_reports)


@admin.route("/game_reports/<int:game_id>/<int:report_id>", methods=['GET', 'POST'])
@wrapper.admin_only
def game_report(game_id: int, report_id: int) -> ResponseReturnValue:
    """
    Render one report of a game.

    :return: rendered template of a report for one game
    :rtype: ResponseReturnValue
    """
    if not models.GameReport.select().where(models.GameReport.game == game_id, models.GameReport.id == report_id).exists():
        abort(404)

    game_report = models.GameReport \
            .select() \
            .join(models.Game) \
            .join(models.User) \
            .where(models.Game.id == game_id, models.GameReport.id == report_id) \
            .get()

    return render_template("game_report.html", game_report = game_report)


@admin.route("/game_reports/<int:game_id>/<report_id>/delete", methods=['GET', 'POST'])
@wrapper.admin_only
def delete_game_report(game_id: int, report_id: int) -> ResponseReturnValue:
    """
    Deletes a game Report.

    :param game_id: id of the game that was reported
    :type game_id: int
    :param report_id: id of the game report to delete
    :type report_id: int

    :return: rendered template of confirmation form for game report deletion
    :rtype: ResponseReturnValue
    """
    if not models.GameReport.select().where(models.GameReport.game == game_id, models.GameReport.id == report_id).exists():
        abort(404)

    if request.method == 'GET':
        game_report = models.GameReport.get(models.GameReport.id == report_id)
        return render_template("delete_game_report.html", game_report = game_report)

    if request.method == 'POST':
        models.GameReport.delete().where(models.GameReport.id == report_id).execute()
        current_app.logger.info(f"The report with the id {report_id} deleted by {session['username']}.")
        return redirect(url_for('admin.reported_games'))


@admin.route("/comment_reports")
@wrapper.admin_only
def reported_comments() -> ResponseReturnValue:
    """
    Render list of all reported comments.

    :return: rendered template of list with all reported items
    :rtype: ResponseReturnValue
    """
    all_reported_comments = models.CommentReport \
            .select(models.Comment.id, fn.COUNT(models.CommentReport.id).alias('number_reports')) \
            .join(models.Comment) \
            .group_by(models.Comment.id)

    return render_template("reported_comments.html", all_reported_comments = all_reported_comments)


@admin.route("/comment_reports/<int:comment_id>")
@wrapper.admin_only
def comment_reports(comment_id: int) -> ResponseReturnValue:
    """
    Render a list of all reports for one comment.

    :return: rendered template of a list with all reports for one comment
    :rtype: ResponseReturnValue
    """
    if not models.Comment.select().where(models.Comment.id == comment_id).exists():
        abort(404)

    all_comment_reports = models.CommentReport \
            .select() \
            .join(models.Comment) \
            .join(models.User) \
            .where(models.Comment.id == comment_id)

    return render_template("comment_reports.html", all_comment_reports = all_comment_reports)


@admin.route("/comment_reports/<int:comment_id>/<int:report_id>", methods=['GET', 'POST'])
@wrapper.admin_only
def comment_report(comment_id: int, report_id: int) -> ResponseReturnValue:
    """
    Render one report of a comment.

    :return: rendered template of a report for one comment
    :rtype: ResponseReturnValue
    """
    if not models.CommentReport.select().where(models.CommentReport.comment == comment_id, models.CommentReport.id == report_id).exists():
        abort(404)

    comment_report = models.CommentReport \
            .select() \
            .join(models.Comment) \
            .join(models.User) \
            .where(models.Comment.id == comment_id, models.CommentReport.id == report_id) \
            .get()

    return render_template("comment_report.html", comment_report = comment_report)


@admin.route("/comment_reports/<int:comment_id>/<report_id>/delete", methods=['GET', 'POST'])
@wrapper.admin_only
def delete_comment_report(comment_id: int, report_id: int) -> ResponseReturnValue:
    """
    Deletes a comment Report.

    :param comment_id: id of the comment that was reported
    :type comment_id: int
    :param report_id: id of the comment report to delete
    :type report_id: int

    :return: rendered template of confirmation form for comment report deletion
    :rtype: ResponseReturnValue
    """
    if not models.CommentReport.select().where(models.CommentReport.comment == comment_id, models.CommentReport.id == report_id).exists():
        abort(404)

    if request.method == 'GET':
        comment_report = models.CommentReport.get(models.CommentReport.id == report_id)
        return render_template("delete_comment_report.html", comment_report = comment_report)

    if request.method == 'POST':
        models.CommentReport.delete().where(models.CommentReport.id == report_id).execute()
        current_app.logger.info(f"The report with the id {report_id} deleted by {session['username']}.")
        return redirect(url_for('admin.reported_comments'))
