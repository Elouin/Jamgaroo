from jamgaroo import models


def parse_categories(categories: str) -> list[str]:
    """
    parse the string of comma seperated categories and put them in a string
    """
    return list(map(lambda x: x.strip(), categories.split(','))) # TODO: Make more pythonic


def create_categories(jam_id: int, categories: str) -> None:
    """
    create database entries for categories if not existent
    and link to jam.
    """
    for category in parse_categories(categories):
        if not models.Category.select().where(models.Category.name == category).exists():
            models.Category(name = category).save()
        models.JamCategory(jam = jam_id, \
                category = models.Category.get(models.Category.name == category).id \
                ).save()
