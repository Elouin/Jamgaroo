from datetime import datetime, timezone
import re

from flask import request, redirect, url_for, session, render_template, Blueprint, current_app, abort, Response, flash
from flask.typing import ResponseReturnValue
from peewee import JOIN, fn
from feedgen.feed import FeedGenerator
from markdown import markdown
from markupsafe import escape

from jamgaroo import models, wrapper
from jamgaroo.jams import categories


jams = Blueprint('jams', __name__, template_folder='templates')


@jams.route("/")
def list_jams() -> ResponseReturnValue:
    """
    Render list of all jams.

    :return: rendered template of list with all jams
    :rtype: ResponseReturnValue
    """
    all_jams = models.Jam.select().order_by(models.Jam.start.desc())
    return render_template("jams.html", all_jams = all_jams)


@jams.route("/<int:jam_id>")
def show_jam(jam_id: int) -> ResponseReturnValue:
    """
    Render page that shows details of one jam.

    :param jam_id: id of the jam to render
    :type jam_id: int

    :return: rendered detail page for jam with given id
    :rtype: ResponseReturnValue
    """
    if not models.Jam.select().where(models.Jam.id == jam_id).exists():
        abort(404)

    jam = models.Jam.select().join(models.User, JOIN.LEFT_OUTER) \
            .join(models.Series, JOIN.LEFT_OUTER) \
            .where(models.Jam.id == jam_id).get()

    jam_categories = models.JamCategory.select().join(models.Category, JOIN.LEFT_OUTER) \
            .where(models.JamCategory.jam == jam_id)

    jam_submissions = models.Game.select().where(models.Game.jam == jam_id)

    best_games = {}

    for category in jam_categories:
        best_games[category.category.name] = models.Rating.select(models.Category.name, models.Game.name, models.Game.id, fn.AVG(models.Rating.points).over(partition_by=[models.Rating.game, models.Rating.category]).alias('points_average')) \
                .join(models.Game) \
                .join_from(models.Rating, models.Category) \
                .where(models.Rating.category == category.category.id, models.Game.jam == jam.id) \
                .order_by(fn.AVG(models.Rating.points).over(partition_by=[models.Rating.game, models.Rating.category]).desc()) \
                .first()
    return render_template("jam.html", jam = jam, \
            jam_categories = jam_categories, \
            jam_submissions = jam_submissions, \
            best_games = best_games, \
            current_date = datetime.now())


@jams.route("/create", methods=['GET', 'POST'])
@wrapper.login_required
def create_jam() -> ResponseReturnValue:
    """
    Renders the page for the creation of jams.

    A Jam has to have a name, description, start date, end date and rating end date.

    :return: rendered template with input fields for jam creation
    :rtype: ResponseReturnValue
    """
    if request.method == 'GET':
        user_series = models.Series.select().join(models.User).where(models.User.name == session['username'])
        return render_template("create_jam.html", user_series = user_series)

    if request.method == 'POST':
        if request.form['name'] and request.form['start'] and request.form['coding'] and request.form['ratings'] and request.form['description'] and request.form['categories']:
            if models.Jam.select().where(models.Jam.name == request.form['name']).exists():
                flash("Creating jam failed, a jam with that name already exists.", "error")
                return render_template("create_jam.html")
            if not re.match(r"^[0-9a-zA-Z\-\ ]{3,25}$", request.form['name']):
                flash("Creating jam failed, name must only contain letters, numbers and '-' and between 3 and 25 chars long.", "error")
                return render_template("create_jam.html")
            jam = models.Jam(name = request.form['name'], \
                    description = request.form['description'], \
                    created_by = models.User.get(models.User.name == session['username']).id, \
                    start = datetime.strptime(request.form['start'], '%Y-%m-%dT%H:%M'), \
                    end_coding = datetime.strptime(request.form['coding'], '%Y-%m-%dT%H:%M'), \
                    end_testing = datetime.strptime(request.form['ratings'], '%Y-%m-%dT%H:%M'))
            if request.form['series']:
                jam.series = models.Series.get(models.Series.name == request.form['series']).id
            jam.save()
            categories.create_categories(models.Jam.get(models.Jam.name == request.form['name']), request.form['categories'])
            current_app.logger.info(f"Jam {request.form['name']} created by {session['username']}.")
            flash("Jam created successfully!", "success")
            return redirect(url_for('jams.show_jam', jam_id = models.Jam.get(models.Jam.name == request.form['name']).id))
        flash("Creating jam failed, all fields except Series are mandatory.", "error")
        return render_template("create_jam.html")


@jams.route("/<int:jam_id>/edit", methods=['GET', 'POST'])
@wrapper.login_required
def edit_jam(jam_id: int) -> ResponseReturnValue:
    """
    Renders a form for editing jams on get and edits jam on post.

    :param jam_id: id of the jam to edit
    :type jam_id: int

    :return: rendered template of form for editing of jams
    :rtype: ResponseReturnValue
    """
    if not models.Jam.select().where(models.Jam.id == jam_id).exists():
        return redirect(url_for('index'))

    if not models.Jam.select().join(models.User).where(models.Jam.id == jam_id).get().created_by.name == session['username']:
        abort(403)

    if request.method == 'GET':
        user_series = models.Series.select().join(models.User).where(models.User.name == session['username'])
        jam = models.Jam.get(models.Jam.id == jam_id)
        return render_template("edit_jam.html", user_series = user_series, jam = jam)

    if request.method == 'POST':
        if request.form['name'] and request.form['start'] and request.form['coding'] and request.form['ratings'] and request.form['description']:
            if models.Jam.select().where(models.Jam.name == request.form['name']).exists() and request.form['name'] != models.Jam.get(models.Jam.id == jam_id).name:
                flash("Updating jam failed, a jam with that name already exists.", "error")
                return render_template("edit_jam.html", jam = models.Jam.get(models.Jam.id == jam_id), jam_id = jam_id)
            if not re.match(r"^[0-9a-zA-Z\-\ ]{3,25}$", request.form['name']):
                flash("Updating jam failed, name must only contain letters, numbers and '-' and between 3 and 25 chars long.", "error")
                return render_template("edit_jam.html", jam = models.Jam.get(models.Jam.id == jam_id), jam_id = jam_id)
            jam = models.Jam.get(models.Jam.id == jam_id)
            jam.name = request.form['name']
            jam.description = request.form['description']
            jam.start = datetime.strptime(request.form['start'], '%Y-%m-%dT%H:%M')
            jam.end_coding = datetime.strptime(request.form['coding'], '%Y-%m-%dT%H:%M')
            jam.end_testing = datetime.strptime(request.form['ratings'], '%Y-%m-%dT%H:%M')
            if request.form['series']:
                jam.series = models.Series.get(models.Series.name == request.form['series']).id
            jam.save()
            current_app.logger.info(f"Jam {request.form['name']} edited by {session['username']}.")
            flash("Jam edited successfully!", "success")
            return redirect(url_for('jams.show_jam', jam_id = jam_id))
        flash("Updating jam failed, all fields except Series are mandatory.", "error")
        return render_template("edit_jam.html", jam = models.Jam.get(models.Jam.id == jam_id))


@jams.route("/<int:jam_id>/delete", methods=['GET', 'POST'])
@wrapper.login_required
def delete_jam(jam_id: int) -> ResponseReturnValue:
    """
    Delete jam.

    :param jam_id: id of the jam to delete
    :type jam_id: int

    :return: rendered template of confirmation form for jam deletion
    :rtype: ResponseReturnValue
    """
    if not models.Jam.select().where(models.Jam.id == jam_id).exists():
        abort(404)
    
    if not models.Jam.select().join(models.User).where(models.Jam.id == jam_id).get().created_by.name == session['username']:
        if not session['username'] in current_app.config["ADMINS"]:
            abort(403)

    if request.method == 'GET':
        jam = models.Jam.get(models.Jam.id == jam_id)
        return render_template("delete_jam.html", jam = jam)

    if request.method == 'POST':
        models.Jam.delete().where(models.Jam.id == jam_id).execute()
        current_app.logger.info(f"Jam {jam_id} deleted by {session['username']}.")
        flash("Jam deleted successfully!", "success")
        return redirect(url_for('index'))


@jams.route("/<int:jam_id>/submit", methods=['GET','POST'])
@wrapper.login_required
def submit_game(jam_id: int) -> ResponseReturnValue:
    """
    Submit game to jam.

    :param jam_id: id of the jam to submit to
    :type jam_id: int

    :return: rendered template of form to submit game
    :rtype: ResponseReturnValue
    """
    if not models.Jam.select().where(models.Jam.id == jam_id).exists():
        abort(404)

    jam = models.Jam.get(models.Jam.id == jam_id)

    if not (jam.start <= datetime.now() and jam.end_coding > datetime.now()):
        return redirect(url_for('.show_jam', jam_id = jam_id))

    if request.method == 'GET':
        return render_template("submit_game.html", jam_id = jam_id)

    if request.method == 'POST':
        if request.form['name'] and request.form['link'] and request.form['description']:
            if not re.match(r"^[0-9a-zA-Z\-\ ]{3,25}$", request.form['name']):
                flash("Updating jam failed, name must only contain letters, numbers and '-' and between 3 and 25 chars long.", "error")
                return render_template("submit_game.html", jam_id = jam_id)
            created_game = models.Game.create(name = request.form['name'], \
                    link = request.form['link'], \
                    description = request.form['description'], \
                    jam = jam_id, \
                    created_by = models.User.get(models.User.name == session['username']).id, \
                    )
            current_app.logger.info(f"Game {request.form['name']} created by {session['username']}.")
            flash("Game successfully submitted!", "success")
            return redirect(url_for('games.show_game', game_id = created_game.id))
        flash("Submitting Game failed, please fill out all fields!", "error")
        return render_template("submit_game.html", jam_id = jam_id)


@jams.route("/<int:jam_id>/report", methods=['GET','POST'])
@wrapper.login_required
def report_jam(jam_id: int) -> ResponseReturnValue:
    """
    Report a jam

    :param jam_id: id of the jam to report
    :type jam_id: int

    :return: rendered template of form to report the jam
    :rtype: ResponseReturnValue
    """
    if not models.Jam.select().where(models.Jam.id == jam_id).exists():
        abort(404)

    jam = models.Jam.get(models.Jam.id == jam_id)

    if request.method == 'GET':
        return render_template("report_jam.html", jam_id = jam_id, jam = jam)

    if request.method == 'POST':
        if request.form['reason']:
            current_app.logger.info(f"Jam {jam.name} was reported by {session['username']}")
            models.JamReport.create(jam = jam_id, \
                    reason = request.form['reason'], \
                    reported_by = models.User.get(models.User.name == session['username']).id)
            flash("Jam reported successfully!", "success")
            return redirect(url_for('jams.show_jam', jam_id = jam_id))
        flash("Reporting jam failed, please provide a reason for reporting.", "error")
        return render_template("report_jam.html", jam_id = jam_id, jam=jam)


@jams.route("/<int:jam_id>/games.rss")
def rss_feed(jam_id: int) -> ResponseReturnValue:
    """
    Creates an RSS feed with all the games from the shown jam.

    :param jam_id: id of the jam to generate an rss feed for
    :type jam_id: int

    :return: rss file with all games in the chosen jam
    :rtype: ResponseReturnValue
    """
    jam = models.Jam.get(models.Jam.id == jam_id)
    game_feed = FeedGenerator()
    game_feed.title(f"{jam.name} feed of games")
    game_feed.author( {'name':current_app.config['NAME'],'email':''} )
    game_feed.link( href=request.root_url[:-1] + url_for('jams.show_jam', jam_id = jam.id), rel='alternate' )
    game_feed.link( href=request.root_url[:-1] + url_for('jams.rss_feed', jam_id = jam.id), rel='self' )
    game_feed.language('en')
    game_feed.description(f"{jam.name} is a gamejam")
    for game in models.Game.select().where(models.Game.jam == jam).order_by(models.Game.created):
        feed_element = game_feed.add_entry()
        feed_element.title(game.name)
        feed_element.link(href=request.root_url[:-1] + url_for('games.show_game', game_id = game.id))
        feed_element.id(request.root_url[:-1] + url_for('games.show_game', game_id = game.id))
        feed_element.description(f"{markdown(escape(game.description))}")
        feed_element.published(game.created.replace(tzinfo=timezone.utc))
    return Response(game_feed.rss_str(pretty=True), mimetype='application/rss+xml')
