import datetime
import re

from flask import Flask, request, redirect, url_for, session, g, render_template, abort, Response, flash
from flask.typing import ResponseReturnValue
from flask_talisman import Talisman
from flask_seasurf import SeaSurf
from passlib.hash import pbkdf2_sha256
from feedgen.feed import FeedGenerator
from markdown import markdown
from peewee import fn
from markupsafe import Markup, escape

from jamgaroo.admin import admin
from jamgaroo.comments import comments
from jamgaroo.games import games
from jamgaroo.jams import jams
from jamgaroo.series import series
from jamgaroo.users import users
from jamgaroo import models


# Intitialize the flask app
app = Flask(__name__)


# register all blueprints
app.register_blueprint(admin, url_prefix='/admin')
app.register_blueprint(comments, url_prefix='/comments')
app.register_blueprint(games, url_prefix='/games')
app.register_blueprint(jams, url_prefix='/jams')
app.register_blueprint(series, url_prefix='/series')
app.register_blueprint(users, url_prefix='/users')


# Initialize security stuff
csp = {
    'default-src': '\'self\'',
    'img-src': 'https://*',
}
Talisman(app, content_security_policy=csp)
csrf = SeaSurf(app)


# Read the config file
app.config.from_pyfile('jamgaroo.cfg')

try:
    app.config.from_envvar('JAMGAROO_SETTINGS')
except RuntimeError:
    app.logger.info("JAMGAROO_SETTINGS envvar not set. Using default settings!")

app.config.from_prefixed_env(prefix = 'JAMGAROO')

# Initialize Database
with app.app_context():
    models.db.create_tables([models.User, models.Series, models.Jam, models.Game, models.Category, models.JamCategory, models.Comment, models.Rating, models.SeriesReport, models.CommentReport, models.GameReport, models.JamReport])


@app.template_filter('markdown')
def markdown_filter(markdown_string: str) -> Markup:
    """
    Filter for the jinja templates, which escapes the markdown and turns it
    into HTML.

    :param markdown_string: markdown string to convert
    :type markdown_string: str

    :return: escaped HTML version of the markdown_string
    :rtype: Markup
    """
    return Markup(markdown(escape(markdown_string)))


@app.before_request
def logged_in() -> None:
    """
    Checks if user is logged in and sets username for context.
    """
    if 'username' in session:
        g.username = session['username']


@app.route("/")
def index() -> ResponseReturnValue:
    """
    Renders the main page with some upcoming and ongoing jams.

    :return: rendered main page template with 3 ongoing and 3 upcoming jams
    :rtype: ResponseReturnValue
    """
    ongoing_jams = models.Jam.select().where((models.Jam.start <= datetime.datetime.now()) & (models.Jam.end_coding > datetime.datetime.now()))
    rating_jams = models.Jam.select().where((models.Jam.end_coding <= datetime.datetime.now()) & (models.Jam.end_testing > datetime.datetime.now()))
    upcoming_jams = models.Jam.select().where(models.Jam.start > datetime.datetime.now())
    return render_template("index.html", ongoing_jams = ongoing_jams, upcoming_jams = upcoming_jams, rating_jams = rating_jams)


@app.route("/about")
def about() -> ResponseReturnValue:
    """
    Renders about page.

    :return: rendered template of about page
    :rtype: ResponseReturnValue
    """
    return render_template("about.html")


@app.route("/tos")
def tos() -> ResponseReturnValue:
    """
    Renders page with Ters of Service.

    :return: rendered template of tos page
    :rtype: ResponseReturnValue
    """
    return render_template("tos.html")


@app.route("/register", methods=['GET', 'POST'])
def register() -> ResponseReturnValue:
    """
    Renders a page for account registration on GET and create account on POST.

    Users have to provide a username, a password and an E-Mail adress.
    Usernames must be between 3 and 12 chars long and may only contain letters, numbers and dashes.

    :return: rendered template of form with all needed fields for account creation
    :rtype: ResponseReturnValue
    """
    if request.method == 'GET':
        return render_template("register.html")
    if request.method == 'POST':
        if app.config['REGISTRATION_DISABLED']:
            abort(403)
        if request.form['username'] and request.form['mail'] and request.form['password'] and request.form['tos']:
            if models.User.select().where(fn.LOWER(models.User.name) == request.form['username'].lower()).exists():
                flash("Registration Failed, username already taken!", "error")
                return render_template("register.html")
            if request.form['username'].lower() in app.config['BLOCKED_USERNAMES']:
                flash("Registration Failed, username is not allowed!", "error")
                return render_template("register.html")
            if models.User.select().where(models.User.email == request.form['mail']).exists():
                flash("Registration Failed, E-Mail adress already linked to an account!", "error")
                return render_template("register.html")
            if not re.match(r"^[0-9a-zA-Z\-]{3,12}$", request.form['username']):
                flash("Registration Failed, username must only contain letters, numbers and '-' and between 3 and 12 chars long.", "error")
                return render_template("register.html")
            models.User(name=request.form['username'], email=request.form['mail'], password=pbkdf2_sha256.hash(request.form['password'])).save()
            app.logger.info(f"Useraccount {request.form['username']} created.")
            flash("Registration successful, you can login now!", "success")
            return redirect(url_for('login'))
        flash("Please fill out all Fields.", "error")
        return render_template("register.html")


@app.route("/login", methods=['GET', 'POST'])
def login() -> ResponseReturnValue:
    """
    Renders a page for login on GET and handle user login on POST.

    If a correct username and password were provided, a session cookie is created, containing the username.
    Redirects to index on successful login.

    :return: rendered template of form for login
    :rtype: ResponseReturnValue
    """
    if request.method == 'GET':
        return render_template("login.html")
    if request.method == 'POST':
        if 'username' in request.form and 'password' in request.form:
            if not models.User.select().where(fn.LOWER(models.User.name) == request.form['username'].lower()).exists():
                flash("Login failed, no such user!", "error")
                return render_template("login.html")
            user = models.User.get(fn.LOWER(models.User.name) == request.form['username'].lower())
            if pbkdf2_sha256.verify(request.form['password'], user.password):
                session.clear()
                session['username'] = user.name
                app.logger.info(f"User {user.name} logged in.")
                return redirect(url_for('index'))
            flash("Login failed, wrong password!", "error")
            return render_template("login.html")


@app.route("/logout")
def logout() -> ResponseReturnValue:
    """
    Logs out users, by clearing the session cookie. Then redirects to index.

    :return: redirect to index page
    :rtype: ResponseReturnValue
    """
    session.clear()
    return redirect(url_for('index'))


@app.route("/jams.rss")
def rss_feed() -> ResponseReturnValue:
    """
    Creates an RSS feed with all the jams on the site.

    :return: rss file with all jams
    :rtype: ResponseReturnValue
    """
    jam_feed = FeedGenerator()
    jam_feed.title(f"{app.config['NAME']}s feed of jams")
    jam_feed.author( {'name':app.config['NAME'],'email':''} )
    jam_feed.link( href=request.root_url[:-1] + url_for('index'), rel='alternate' )
    jam_feed.link( href=request.root_url[:-1] + url_for('rss_feed'), rel='self' )
    jam_feed.language('en')
    jam_feed.description(f"{app.config['NAME']} is a webservice to host your gamejams")
    for jam in models.Jam.select().order_by(models.Jam.created):
        feed_element = jam_feed.add_entry()
        feed_element.title(jam.name)
        feed_element.link(href=request.root_url[:-1] + url_for('jams.show_jam', jam_id = jam.id))
        feed_element.id(request.root_url[:-1] + url_for('jams.show_jam', jam_id = jam.id))
        feed_element.description(f"Starts at: {jam.start}<br>Coding until: {jam.end_coding}<br>Ratings until: {jam.end_testing}{markdown(escape(jam.description))}")
        feed_element.published(jam.created.replace(tzinfo=datetime.timezone.utc))
    return Response(jam_feed.rss_str(pretty=True), mimetype='application/rss+xml')


@app.errorhandler(404)
def page_not_found(e) -> ResponseReturnValue:
    """
    Custom error page for 404 errors.
    """
    return render_template('404.html'), 404


@app.errorhandler(403)
def forbidden(e) -> ResponseReturnValue:
    """
    Custom error page for 403 errors.
    """
    return render_template('403.html'), 403
