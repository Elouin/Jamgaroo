import re
import datetime

from flask import request, redirect, url_for, session, render_template, Blueprint, g, current_app, abort, flash
from flask.typing import ResponseReturnValue
from peewee import JOIN

from jamgaroo import models, wrapper


games = Blueprint('games', __name__, template_folder='templates')


@games.route("/")
def list_games() -> ResponseReturnValue:
    """
    Renders a list of all games.

    :return: rendered template of games list
    :rtype: ResponseReturnValue
    """
    all_games = models.Game.select()
    return render_template("games.html", all_games = all_games)


@games.route("/<int:game_id>")
def show_game(game_id: int) -> ResponseReturnValue:
    """
    Renders a page that shows the deatails one game.

    :param game_id: id of the game to show
    :type game_id: int

    :return: rendered template of game details
    :rtype: ResponseReturnValue
    """
    if not models.Game.select().where(models.Game.id == game_id).exists():
        abort(404)

    ratings = models.Rating.select().join(models.Category).join_from(models.Rating, models.User).where(models.Rating.game == game_id)
    comments = models.Comment.select().join(models.User).where(models.Comment.game == game_id)
    user_ratings = {}
    user_comments = {}

    for rating in ratings:
        try:
            user_ratings[rating.user.name][rating.category.name] = rating.points
        except(KeyError):
            user_ratings[rating.user.name] = {}
            user_ratings[rating.user.name][rating.category.name] = rating.points
    for comment in comments:
        user_comments[comment.user.name] = comment
    return render_template("game.html", game = models.Game.select().join(models.User).join_from(models.Game, models.Jam).where(models.Game.id == game_id).get(), \
            user_ratings = user_ratings,
            user_comments = user_comments,
            current_date = datetime.datetime.now())


@games.route("/<game_id>/edit", methods=['GET', 'POST'])
@wrapper.login_required
def edit_game(game_id: int) -> ResponseReturnValue:
    """
    Edits a game.

    :param game_id: id of the game to edit
    :type game_id: int

    :return: rendered template of form for editing of game
    :rtype: ResponseReturnValue
    """
    if not models.Game.select().where(models.Game.id == game_id).exists():
        abort(404)

    if request.method == 'GET':
        game = models.Game.get(models.Game.id == game_id)
        return render_template("edit_game.html", game = game)

    if not models.Game.select().join(models.User).where(models.Game.id == game_id).get().created_by.name == session['username']:
        abort(403)

    if request.method == 'POST':
        if request.form['name'] and request.form['description']:
            if models.Game.select().where(models.Game.name == request.form['name']).exists() and request.form['name'] != models.Game.get(models.Game.id == game_id).name:
                flash("Editing game failed, a game with that name already exists.", "error")
                return render_template("edit_game.html", game_id = game_id, game = models.Game.get(models.Game.id == game_id))
            if not re.match(r"^[0-9a-zA-Z\-\ ]{3,25}$", request.form['name']):
                flash("Editing game failed, name must only contain letters, numbers and '-' and between 3 and 25 chars long.", "error")
                return render_template("edit_game.html", game = models.Game.get(models.Game.id == game_id), game_id = game_id)
            models.Game.update(name=request.form['name'], \
                    description = request.form['description'], \
                    link = request.form['link'], \
                    ).where(models.Game.id == game_id).execute()
            current_app.logger.info(f"Game {request.form['name']} edited by {session['username']}.")
            flash("Game successfully edited!", "success")
            return redirect(url_for('games.show_game', game_id = game_id))
        flash("Editing game failed, please fill out all fields.", "error")
        return render_template("edit_game.html", game = models.Game.get(models.Game.id == game_id), game_id = game_id)


@games.route("/<int:game_id>/delete", methods=['GET', 'POST'])
@wrapper.login_required
def delete_game(game_id: int) -> ResponseReturnValue:
    """
    Deletes a game.

    :param game_id: id of the game to delete
    :type game_id: int

    :return: rendered template of confirmation form for game deletion
    :rtype: ResponseReturnValue
    """
    if not models.Game.select().where(models.Game.id == game_id).exists():
        abort(404)

    if not models.Game.select().join(models.User).where(models.Game.id == game_id).get().created_by.name == session['username']:
        if not session['username'] in current_app.config["ADMINS"]:
            abort(403)

    if request.method == 'GET':
        game = models.Game.get(models.Game.id == game_id)
        return render_template("delete_game.html", game = game)

    if request.method == 'POST':
        models.Game.delete().where(models.Game.id == game_id).execute()
        current_app.logger.info(f"Game with the id {game_id} deleted by {session['username']}.")
        flash("Game successfully deleted!", "success")
        return redirect(url_for('index'))


@games.route("/<int:game_id>/rate", methods=['GET', 'POST'])
@wrapper.login_required
def rate_game(game_id: int) -> ResponseReturnValue:
    """
    Creates a game rating a game.

    :param game_id: id of the game to rate
    :type game_id: int

    :return: rendered template of confirmation form for game rating
    :rtype: ResponseReturnValue
    """
    if not models.Game.select().where(models.Game.id == game_id).exists():
        abort(404)

    if models.Comment.select().join(models.User).where(models.Comment.game == game_id, models.Comment.user == models.User.get(models.User.name == session['username'])).exists():
        return redirect(url_for('games.show_game', game_id = game_id))

    if request.method == 'GET':
        game = models.Game.get(models.Game.id == game_id)
        jam_categories = models.JamCategory.select().join(models.Category, JOIN.LEFT_OUTER) \
                .where(models.JamCategory.jam == game.jam)
        return render_template("rate_game.html", jam_categories = jam_categories, game = game)

    if request.method == 'POST':
        game = models.Game.get(models.Game.id == game_id)
        jam_categories = models.JamCategory.select().join(models.Category, JOIN.LEFT_OUTER) \
                .where(models.JamCategory.jam == game.jam)
        category_names = [category.category.name for category in jam_categories]
        for item in request.form:
            if request.form[item]:
                if item == 'comment':
                    models.Comment.create(comment = request.form['comment'], \
                            user = models.User.get(models.User.name == g.username).id, \
                            game = game_id)
                if item in category_names:
                    models.Rating.create(points = request.form[item], \
                            user = models.User.get(models.User.name == g.username).id, \
                            game = game_id, \
                            category = models.Category.get(models.Category.name == item).id)
            else:
                flash("Submitting review failed, please fill out all fields.", "error")
                return render_template("rate_game.html", game_id = game_id)
        current_app.logger.info(f"Game {game_id} rated by {session['username']}.")
        flash("Rating successfully submitted!", "success")
        return redirect(url_for('games.show_game', game_id = game_id))


@games.route("/<int:game_id>/report", methods=['GET','POST'])
@wrapper.login_required
def report_game(game_id: int) -> ResponseReturnValue:
    """
    Report a game

    :param game_id: id of the game to report
    :type game_id: int

    :return: rendered template of form to report the game
    :rtype: ResponseReturnValue
    """
    if not models.Game.select().where(models.Game.id == game_id).exists():
        abort(404)

    game = models.Game.get(models.Game.id == game_id)

    if request.method == 'GET':
        return render_template("report_game.html", game_id = game_id, game = game)

    if request.method == 'POST':
        if request.form['reason']:
            current_app.logger.info(f"Game {game.name} was reported by {session['username']}")
            models.GameReport.create(game = game_id, \
                    reason = request.form['reason'], \
                    reported_by = models.User.get(models.User.name == session['username']).id)
            flash("Game successfully reported!", "success")
            return redirect(url_for('games.show_game', game_id = game_id))
        flash("Reporting game failed, please provide a reason for reporting.", "error")
        return render_template("report_game.html", game_id = game_id, game=game)
